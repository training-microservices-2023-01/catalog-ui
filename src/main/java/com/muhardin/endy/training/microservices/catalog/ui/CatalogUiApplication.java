package com.muhardin.endy.training.microservices.catalog.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CatalogUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogUiApplication.class, args);
	}
}
