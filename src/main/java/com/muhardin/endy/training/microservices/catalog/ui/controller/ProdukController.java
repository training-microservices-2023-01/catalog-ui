package com.muhardin.endy.training.microservices.catalog.ui.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.muhardin.endy.training.microservices.catalog.ui.service.ProdukBackendService;
import com.muhardin.endy.training.microservices.catalog.ui.service.ProdukBackendServiceManual;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller @RequestMapping("/produk")
public class ProdukController {

    @Autowired private ProdukBackendService produkBackendService;
    @Autowired 
    private ProdukBackendServiceManual produkBackendServiceManual;

    @GetMapping("/list")
    public void dataProduk(ModelMap modelMap){
        log.info("Menjalankan method list product");
        modelMap.addAttribute("dataProduk", produkBackendService.dataProduk());
    }

    @GetMapping("/list2")
    public String dataProduk2(ModelMap modelMap, @RegisteredOAuth2AuthorizedClient("catalog") OAuth2AuthorizedClient authorizedClient){
        log.info("Menjalankan method list product");
        modelMap.addAttribute("dataProduk", produkBackendServiceManual.dataProduk(authorizedClient));
        return "produk/list";
    }

    @GetMapping("/backend")
    @ResponseBody
    public Map<String, Object> backendInfo(){
        log.info("Menjalankan method info backend");
        return produkBackendService.infoBackend();
    }

}
