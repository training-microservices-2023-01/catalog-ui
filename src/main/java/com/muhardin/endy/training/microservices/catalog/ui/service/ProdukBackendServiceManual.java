package com.muhardin.endy.training.microservices.catalog.ui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.muhardin.endy.training.microservices.catalog.ui.dto.Produk;

@Service
public class ProdukBackendServiceManual {
    private static final String URI_LIST_TRANSAKSI = "/api/produk/";

    @Autowired private WebClient webClient;


    public List<Produk> dataProduk(OAuth2AuthorizedClient authorizedClient){
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
            .path(URI_LIST_TRANSAKSI)
            .build())
        .attributes(ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient(authorizedClient))
        .retrieve()
        .bodyToMono(new ParameterizedTypeReference<List<Produk>>(){})
        .block();
    }
}
