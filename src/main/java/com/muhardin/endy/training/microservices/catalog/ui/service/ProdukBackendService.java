package com.muhardin.endy.training.microservices.catalog.ui.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import com.muhardin.endy.training.microservices.catalog.ui.dto.Produk;
import com.muhardin.endy.training.microservices.catalog.ui.service.ProdukBackendService.ProdukFallback;

import lombok.extern.slf4j.Slf4j;

@FeignClient(value = "catalog")
public interface ProdukBackendService {
    
    @GetMapping("/api/produk/")
    public List<Produk> dataProduk();

    @GetMapping("/api/info")
    public Map<String, Object> infoBackend();

    @Component @Slf4j
    static class ProdukFallback implements ProdukBackendService {

        @Override
        public List<Produk> dataProduk() {
            log.info("Menjalankan fallback method dataProduk");
            return new ArrayList<>();
        }

        @Override
        public Map<String, Object> infoBackend() {
            log.info("Menjalankan fallback method infoBackend");
            return new HashMap<>();
        }

    }
}
