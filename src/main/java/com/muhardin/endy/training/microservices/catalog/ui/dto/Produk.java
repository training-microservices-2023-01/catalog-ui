package com.muhardin.endy.training.microservices.catalog.ui.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Produk {
    private String id;
    private String kode;
    private String nama;
    private BigDecimal harga;
}
